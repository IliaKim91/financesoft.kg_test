# Financesoft.kg C# 1-й этап

Мои ответы на задания C# 1-ого этапа от FinanceSoft.

## ReverseString

Класс [Reverse](https://gitlab.com/IliaKim91/financesoft.kg_test/-/tree/main/ReverseStringTask/ReverseString), который содержит два метода:

- [ ] ReverseStrUseWhile
- [ ] ReverseStrUseFor

Оба метода «переворачивают» строку. Т.е. из строки abcd получать строку dcba.

## EncodeString (SHA1String)

Отвечает за кодирование строки. Использует кодирование паролей SHA-1, а также метода имплементированного в задании. Предоставляется два варианта на выбор для кодирования. Имплементировано два варианта функции [EncodeString](https://gitlab.com/IliaKim91/financesoft.kg_test/-/tree/main/SHA1StringTask):

- [ ] EncodeString
- [ ] EncodeStringV2

Прошу прощения за название 2-ого метода и прошу строго не судить. Буду рад комментариям и замечаниям моего кода.

## Benchmark tests

Тест для сравнения быстродействиности двух методов в классе Reverse. Для запуска теста [Benchmark](https://gitlab.com/IliaKim91/financesoft.kg_test/-/tree/main/ReverseStringTask/Benchmark) надо перевести приложение из режима Debug в режим Release.

BenchmarkDotNet=v0.13.4, OS=Windows 10 (10.0.17763.253/1809/October2018Update/Redstone5)
Intel Core i5-8250U CPU 1.60GHz (Kaby Lake R), 1 CPU, 8 logical and 4 physical cores
.NET SDK=7.0.100

       |                          Method |     Mean |    Error |   StdDev | Rank |   Gen0 | Allocated |
       |-------------------------------- |---------:|---------:|---------:|-----:|-------:|----------:|
       |   ReverseStrUseForBenchmarkTest | 77.01 ns | 1.548 ns | 1.957 ns |    2 | 0.1070 |     336 B |
       | ReverseStrUseWhileBenchmarkTest | 71.10 ns | 0.157 ns | 0.131 ns |    1 | 0.1070 |     336 B |

## XUnit tests

XUnit тесты для проверки работы методов: 

- [ ] [SHA1StringTests](https://gitlab.com/IliaKim91/financesoft.kg_test/-/tree/main/SHA1StringTask/SHA1StringTests)
- [ ] [ReverseStringTests](https://gitlab.com/IliaKim91/financesoft.kg_test/-/tree/main/ReverseStringTask/ReverseStringTests)
